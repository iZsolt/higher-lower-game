﻿using Xamarin.Forms;
using System.Windows.Input;
using HigherLower.Helpers;

namespace HigherLower.ViewModels
{
    public class MenuPageViewModel
    {

        #region constructor
        public MenuPageViewModel()
        {
            StartVehicleGame = new Command(VehicleOnTapped);
            StartInventionGame = new Command(InventionOnTapped);
        }
        #endregion

        #region commands
        public ICommand StartVehicleGame { get; }
        
        public ICommand StartInventionGame { get; }

        #endregion

        #region methods

        /// <summary>
        /// Start a new game, with Vehicles
        /// </summary>
        private void VehicleOnTapped ()
        {
            Mediator.Instance.OpenNewGamePage("Vehicle");
        }

        /// <summary>
        /// Start a new game with inventions
        /// </summary>
        private void InventionOnTapped()
        {
            Mediator.Instance.OpenNewGamePage("Invention");
            
        }

        #endregion
    }
}
