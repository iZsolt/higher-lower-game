﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using HigherLower.Models;
using Xamarin.Forms;
using System.Windows.Input;
using Xamarin.Essentials;
using HigherLower.Helpers;
using HigherLower.Interfaces;

namespace HigherLower.ViewModels
{

    public class GamePageVM : NotifyPropertyChangedBase
    {

        #region fields
        private ObservableCollection<ObjectModelBase> _gameObjects = new ObservableCollection<ObjectModelBase>();
        private ObjectModelBase _knownObject;
        private ObjectModelBase _guessebObject;
        private short _score;
        private int _highScore;
        private bool _hiloButtonsVisible;
        private string _gameType;
        private string _lastDisplayedString;
        #endregion

        #region constructor
        public GamePageVM()
        {
            BackCommand = new Command(BackToMenu);
            HigherInput = new Command(() =>
            {
                if (GuessedObject.Value >= KnownObject.Value)
                {
                    ProcedureCorrect();
                }
                else
                {
                    ProcedureIncorrect();
                }
            });

            LowerInput = new Command(() =>
            {
                if (GuessedObject.Value < KnownObject.Value)
                {
                    ProcedureCorrect();
                }
                else
                {
                    ProcedureIncorrect();
                }
            });

        }
        #endregion

        #region properties

        public string LastDisplayedString
        {
            get
            {
                return _lastDisplayedString;
            }
            set
            {
                _lastDisplayedString = value;
                OnPropertyChanged("LastDisplayedString");
            }
        }

        public IView View { get; set; }

        public ObservableCollection<ObjectModelBase> GameObjects
        {
            get
            {
                return _gameObjects;
            }
            set
            {
                _gameObjects = value;
                OnPropertyChanged("GameObjects");
            }
        }

        public int HighScore
        {
            get
            {
                return _highScore;
            }
            set
            {
                _highScore = value;
                OnPropertyChanged("HighScore");
            }
        }

        public short Score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
                OnPropertyChanged("Score");
            }
        }

        public bool HiLoButtonsVisible
        {
            get
            {
                return _hiloButtonsVisible;
            }
            set
            {
                _hiloButtonsVisible = value;
                OnPropertyChanged("HiLoButtonsVisible");
            }
        }

        public ObjectModelBase KnownObject
        {
            get
            {
                return _knownObject;
            }
            set
            {
                _knownObject = value;
                OnPropertyChanged("KnownObject");
            }
        }
        public ObjectModelBase GuessedObject
        {
            get
            {
                return _guessebObject;
            }
            set
            {
                _guessebObject = value;
                OnPropertyChanged("GuessedObject");
            }
        }

        public string GameType
        {
            get
            {
                return _gameType;
            }
            set
            {
                _gameType = value;
                OnPropertyChanged("GameType");
            }
        }

        public string UpperButtonText { get; set; }
        public string LowerButtonText { get; set; }

        public string UnitMeasureDisplayed { get; set; }
        public string ThingMeasured { get; set; }

        #endregion

        #region commands
        public ICommand BackCommand { get; }

        public ICommand HigherInput { get; }
        public ICommand LowerInput { get; }

        #endregion

        #region public methods

        /// <summary>
        /// Initializes the VM, sets default values
        /// </summary>
        /// <param name="obj">Type of game started</param>
        /// <returns></returns>
        public async Task SetGameData(string obj)
        {
            GameType = obj;
            GameObjects = new ObservableCollection<ObjectModelBase>(await DataBaseAcces.Instance.GetGameObjects(obj));
            switch (obj)
            {
                case "Vehicle":
                    UnitMeasureDisplayed = "Km/h";
                    ThingMeasured = "Top speed";
                    UpperButtonText = "HIGHER ↑";
                    LowerButtonText = "LOWER ↓";
                    HighScore = Preferences.Get("VehicleHighScore", 0);
                    LastDisplayedString = String.Format("Than {0} of", ThingMeasured);
                    break;
                case "Invention":
                    UnitMeasureDisplayed = "";
                    ThingMeasured = "Invention year";
                    UpperButtonText = "AFTER";
                    LowerButtonText = "BEFORE";
                    HighScore = Preferences.Get("InventionHighScore", 0);
                    LastDisplayedString = String.Format("The {0} of", ThingMeasured);
                    break;
                default:
                    UnitMeasureDisplayed = null;
                    ThingMeasured = null;
                    break;
            }
            KnownObject = SelectRandomObject();
            GuessedObject = SelectRandomObject();
            Score = 0;
            HiLoButtonsVisible = true;
        }
        #endregion

        #region private methods

        /// <summary>
        /// If confirmed, pops the current page off the navigation stack
        /// </summary>
        private async void BackToMenu()
        {
            bool answer = await Application.Current.MainPage.DisplayAlert("Return to main menu?", "Your current game will end", "Ok", "Cancel");
            if (answer)
            {
                Mediator.Instance.BackToMenu();
            }
        }

        /// <summary>
        /// Removes a given object from the GameObjects list
        /// </summary>
        /// <param name="obj"></param>
        private void RemoveFromObjectList(ObjectModelBase obj)
        {
            GameObjects.Remove(obj);
        }

        /// <summary>
        /// Selects a random object from the GameObjects, and also removes the object from the list
        /// </summary>
        /// <returns></returns>
        private ObjectModelBase SelectRandomObject()
        {
            int size = GameObjects.Count;
            int rnd = new Random().Next(size);
            ObjectModelBase myObj = GameObjects.ElementAt(rnd);
            RemoveFromObjectList(myObj);
            return myObj;
        }

        /// <summary>
        /// Procedure when input is correct
        /// </summary>
        private async void ProcedureCorrect()
        {
            HiLoButtonsVisible = false;//Buttons disappear
            await View.ShowResult();
            await View.ToggleResponseOk();
            Score++;
            await View.ScoreAnimation();
            if (GameObjects.Count > 0)
            {
                await View.ToggleResponseOk();
                await View.ToggleUpperPart();
                KnownObject = GuessedObject;
                GuessedObject = SelectRandomObject();
                await View.ToggleUpperPart();
                HiLoButtonsVisible = true;
            }
            else
            {
                OpenResultPage();
            }
        }

        /// <summary>
        /// Procedure when the input is incorrect
        /// </summary>
        private async void ProcedureIncorrect()
        {
            HiLoButtonsVisible = false;
            await View.ShowResult();
            await View.ToggleResponseWrong();
            OpenResultPage();

        }

        /// <summary>
        /// Opens the result page, when game is finished
        /// </summary>
        private void OpenResultPage()
        {
            Mediator.Instance.OpenResultPage(Score, GameType);
        }

        #endregion
    }
}
