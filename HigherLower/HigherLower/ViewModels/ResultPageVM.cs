﻿using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using HigherLower.Helpers;

namespace HigherLower.ViewModels
{
    public class ResultPageVM : NotifyPropertyChangedBase
    {
        #region fields
        private short _score;
        private int _highScore;
        private string _gameType;
        #endregion
        #region constructor
        public ResultPageVM()
        {
            BackToMenu = new Command( () =>
              {
                  Mediator.Instance.BackToMenu();
              });

            StartNewGame = new Command(NewGame);
        }
        #endregion

        #region properties
        public short Score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
                OnPropertyChanged("Score");
            }
        }

        public int HighScore
        {
            get
            {
                return _highScore;
            }
            set
            {
                _highScore = value;
                OnPropertyChanged("HighScore");
            }
        }

        public string GameType
        {
            get
            {
                return _gameType;
            }
            set
            {
                _gameType = value;
                OnPropertyChanged("GameType");
            }
        }
        #endregion

        #region commands
        public ICommand BackToMenu { get; }

        public ICommand StartNewGame { get; }
        #endregion

        #region public methods

        /// <summary>
        /// Sets the appropiate score, high score values
        /// </summary>
        /// <param name="score">Score reached in latest game</param>
        public void SetGameData(short score, string gameType)
        {
            Score = score;

            GameType = gameType;

            switch (GameType)
            {
                case "Vehicle":
                    HighScore = Preferences.Get("VehicleHighScore", 0);
                    if (Score > HighScore)
                    {
                        Preferences.Set("VehicleHighScore", Score);
                        HighScore = Score;
                    }
                    break;
                case "Invention":
                    HighScore = Preferences.Get("InventionHighScore", 0);
                    if (Score > HighScore)
                    {
                        Preferences.Set("InventionHighScore", Score);
                        HighScore = Score;
                    }
                    break;
                default:
                    break;
            }

        }
        #endregion

        #region private methods
        /// <summary>
        /// Method to start a new game of the same type
        /// </summary>
        private void NewGame()
        {
            Mediator.Instance.OpenNewGamePage(GameType, true);
        }

        #endregion
    }
}
