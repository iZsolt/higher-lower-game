﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HigherLower.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultPage : ContentPage
    {
        public ResultPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Disables the built-in(hardware) back button
        /// </summary>
        /// <returns></returns>
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}