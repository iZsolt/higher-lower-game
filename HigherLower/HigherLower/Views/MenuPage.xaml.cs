﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HigherLower
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
        }

    }
}