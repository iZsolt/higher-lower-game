﻿using HigherLower.Interfaces;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HigherLower.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage, IView
    {
        #region fields
        static Page currentpage;
        static Label score;
        static Label middleLabel;
        static Frame middleFrame;
        static StackLayout resultStack;
        static Image upperImage;
        static StackLayout upperStackLayout;

        #endregion

        #region constructor
        public GamePage()
        {
            InitializeComponent();
        }

        #endregion

        #region properties 
        #endregion

        #region methods

        public async Task ShowResult()
        {
            resultStack.Opacity = 0;
            await resultStack.FadeTo(1, 1000, Easing.SinInOut);

        }

        public async Task ToggleResponseOk()
        {

            if (middleLabel.Text == "VS")
            {
                await middleFrame.FadeTo(0, 300, Easing.Linear);
                middleLabel.Text = "OK";
                middleLabel.TextColor = Color.White;
                middleFrame.BackgroundColor = Color.Green;
                await middleFrame.FadeTo(1, 300, Easing.Linear);
            }
            else
            {
                await middleFrame.FadeTo(0, 300, Easing.Linear);
                middleLabel.Text = "VS";
                middleLabel.TextColor = Color.Black;
                middleFrame.BackgroundColor = Color.White;
                await middleFrame.FadeTo(1, 300, Easing.Linear);
            }

        }


        public async Task ToggleResponseWrong()
        {

            if (middleLabel.Text == "VS")
            {
                await middleFrame.FadeTo(0, 400, Easing.Linear);
                middleLabel.Text = "X";
                middleLabel.TextColor = Color.White;
                middleFrame.BackgroundColor = Color.Red;
                await middleFrame.FadeTo(1, 400, Easing.Linear);
            }
            else
            {
                await middleFrame.FadeTo(0, 400, Easing.Linear);
                middleLabel.Text = "VS";
                middleLabel.TextColor = Color.Black;
                middleFrame.BackgroundColor = Color.White;
                await middleFrame.FadeTo(1, 400, Easing.Linear);
            }
            await Task.Delay(150);

        }

        /// <summary>
        /// Sets the current page, needed for animations
        /// </summary>
        public void SetCurrentPage()
        {
            currentpage = Application.Current.MainPage.Navigation.NavigationStack.Last();
            score = currentpage.FindByName<Label>("ScoreLabel");
            middleLabel = currentpage.FindByName<Label>("MiddleLabel");
            middleFrame = currentpage.FindByName<Frame>("MiddleFrame");
            resultStack = currentpage.FindByName<StackLayout>("ResultStackLayout");
            upperImage = currentpage.FindByName<Image>("UpperImage");
            upperStackLayout = currentpage.FindByName<StackLayout>("UpperStackLayout");
        }

        public async Task ScoreAnimation()
        {
            await score.TranslateTo(-20, 0, 50);
            await score.ScaleTo(1.5, 200);
            await Task.Delay(250);
            await score.ScaleTo(1.0, 200);
            await Task.Delay(50);
            await score.TranslateTo(0, 0, 50);
        }

        public async Task ToggleUpperPart()
        {
            if (upperImage.Opacity == 1)
            {
                upperStackLayout.IsVisible = false;
                await upperImage.FadeTo(0, 1000, Easing.SinInOut);
            }
            else
            {
                upperImage.Opacity = 1;
                upperStackLayout.IsVisible = true;
            }
        }

        #endregion

        #region NavigatonManipulation
        /// <summary>
        /// Disables the built-in(hardware) back button
        /// </summary>
        /// <returns></returns>
        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        #endregion
    }
}