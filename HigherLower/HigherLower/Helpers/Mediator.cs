﻿using Xamarin.Forms;
using HigherLower.ViewModels;
using HigherLower.Views;
using System.Linq;

namespace HigherLower.Helpers
{
    public class Mediator
    {
        #region fields
        private static Mediator _instance;
        private static readonly object _locker = new object();
        #endregion

        #region constructor
        private Mediator()
        {

        }
        #endregion

        #region properties

        public static Mediator Instance
        {
            get
            {
                lock (_locker)
                {
                    if (_instance == null)
                    {
                        _instance = new Mediator();
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// Removes every page, except the menu page
        /// </summary>
        public async void BackToMenu()
        {
            await Application.Current.MainPage.Navigation.PopToRootAsync();
        }

        /// <summary>
        /// Start new game of the given type
        /// </summary>
        /// <param name="gameType">Game type to start</param>
        /// <param name="calledFromResultPage">Method called from result page</param>
        public async void OpenNewGamePage(string gameType, bool calledFromResultPage=false)
        {
            GamePageVM NewGameVM = new GamePageVM();

            if (calledFromResultPage)
            {
                RemoveAllGamePage();
            }

            switch (gameType)
            {
                case "Vehicle":
                    await NewGameVM.SetGameData("Vehicle");
                    break;
                case "Invention":
                    await NewGameVM.SetGameData("Invention");
                    break;
                default:
                    break;
            }

            GamePage NewGamePage = new GamePage();

            NewGamePage.BindingContext = NewGameVM;

            NewGameVM.View = NewGamePage;

            await Application.Current.MainPage.Navigation.PushAsync(NewGamePage);

            NewGamePage.SetCurrentPage();
        }

        /// <summary>
        /// Remove given page from navigation stack
        /// </summary>
        /// <param name="page"></param>
        public void RemovePageFromStack(Page page)
        {
            Application.Current.MainPage.Navigation.RemovePage(page);
        }

        /// <summary>
        /// Open result page
        /// </summary>
        /// <param name="score">Latest score</param>
        /// <param name="gameType">Latest game type</param>
        public async void OpenResultPage(short score, string gameType)
        {
            var resultPageVM = new ResultPageVM();
            resultPageVM.SetGameData(score, gameType);

            var resultPage = new ResultPage();

            resultPage.BindingContext = resultPageVM;

            await Application.Current.MainPage.Navigation.PushAsync(resultPage);
        }

        /// <summary>
        /// Remove every game page from the navigation stack
        /// </summary>
        private void RemoveAllGamePage()
        {
            for (int i = 0; i < Application.Current.MainPage.Navigation.NavigationStack.Count; i++)
            {
                if (Application.Current.MainPage.Navigation.NavigationStack.ElementAt(i) is GamePage)
                {
                    Application.Current.MainPage.Navigation.RemovePage(Application.Current.MainPage.Navigation.NavigationStack.ElementAt(i));
                }
            }
        }
        #endregion

    }
}
