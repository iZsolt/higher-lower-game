﻿using Xamarin.Forms;

namespace HigherLower.Helpers
{
    /// <summary>
    /// Helps to change the background color of a button, when pressed
    /// </summary>
    public class ButtonTriggerAction : TriggerAction<VisualElement>
    {
        public Color BackgroundColor { get; set; }

        protected override void Invoke(VisualElement visual)
        {
            Button button = visual as Button;
            if (button == null) return;
            if (BackgroundColor != null) button.BackgroundColor = BackgroundColor;
        }
    }
}
