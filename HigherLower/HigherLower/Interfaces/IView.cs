﻿using System.Threading.Tasks;


namespace HigherLower.Interfaces
{
    /// <summary>
    /// Interface for game pages
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// Animation for the score label
        /// </summary>
        /// <returns></returns>
        Task ScoreAnimation();

        /// <summary>
        /// Animation for the upper part of the screen, on toggle appears or disappears
        /// </summary>
        /// <returns></returns>
        Task ToggleUpperPart();

        /// <summary>
        /// Animation for the result to show
        /// </summary>
        /// <returns></returns>
        Task ShowResult();

        /// <summary>
        /// Animation for wrong input
        /// </summary>
        /// <returns></returns>
        /// 
        Task ToggleResponseWrong();
        /// <summary>
        /// Animation for right input
        /// </summary>
        /// <returns></returns>
        Task ToggleResponseOk();
    }
}
