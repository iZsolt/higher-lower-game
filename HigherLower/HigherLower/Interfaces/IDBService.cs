﻿using SQLite;

namespace HigherLower.Interfaces
{
    public interface IDbService
    {
        SQLiteAsyncConnection CreateConnection();
    }
}
