﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using HigherLower.Models;
using System.Linq;
using SQLite;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HigherLower
{
    public sealed class DataBaseAcces
    {
        #region fields
        private static DataBaseAcces _instance;
        private static readonly object _locker = new object();
        public static string dbName = "myData.db";
        public static string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), dbName);
        #endregion
        #region constructor
        private DataBaseAcces()
        {

        }
        #endregion
        #region properties
        public static DataBaseAcces Instance
        {
            get
            {
                lock (_locker)
                {
                    if (_instance == null)
                    {
                        _instance = new DataBaseAcces();
                    }
                }
                return _instance;
            }
        }
        #endregion
        #region methods

        /// <summary>
        /// Returns a list of objects of selected game type from the database
        /// </summary>
        /// <returns></returns>
        public async Task<List<ObjectModelBase>> GetGameObjects(string sample)
        {
            List<ObjectModelBase> objs = new List<ObjectModelBase>();
            SQLiteAsyncConnection conn = null;
            try
            {
                conn = App.DbService.CreateConnection();
                {
                    switch (sample)
                    {
                        case "Vehicle":
                            var myVehs = await conn.Table<Vehicle>().ToListAsync();
                            foreach (var item in myVehs)
                            {
                                objs.Add(item);
                            }
                            break;

                        case "Invention":
                            var myInv = await conn.Table<Invention>().ToListAsync();
                            foreach (var item in myInv)
                            {
                                objs.Add(item);
                            }
                            break;

                        default:
                            objs = null;
                            break;
                    }

                    return objs;
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Alert", "Database could not be loaded!", "Cancel");
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            finally
            {
                conn?.CloseAsync();
            }
            return null;
        }



        #endregion
    }
}
