﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.IO;
using HigherLower.Interfaces;

namespace HigherLower
{
    public partial class App : Application
    {
        public static IDbService DbService { get; private set; }
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MenuPage());
        }

        public static void SetDbService(IDbService service)
        {
            DbService = service;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

    }
}
