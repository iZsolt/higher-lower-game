﻿using SQLite;
using Xamarin.Forms;

namespace HigherLower.Models

{
    /// <summary>
    /// Base for various game type objects
    /// </summary>
    public abstract class ObjectModelBase : BindableObject
    {
        [Column("Name")]
        public string Name { get; set; }
        [Column("Value")]
        public int Value { get; set; }

        [Column("ImgSource")]
        public string ImageSource { get; set; }

        [Column("ID")]
        [PrimaryKey]
        [AutoIncrement]
        public int ID { get; set; }
    }
}
